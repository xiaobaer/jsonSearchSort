/**
jsonSearchSort.js  v0.1
by yaolingkun 
str：搜索字符串   例如"  ";" 汉字";" 汉 字 ";"字母（大小写）";"数字";"特殊字符";
json：数据源  [{"id": 1, "name": "张三","age":"25"}, {"id": 2, "name": "李四","age":"35"}]
options：样式 例如{color : "#F00", "font-size" : "150%"}
*/
(function($){ 
    $.fn.jsonSearchSort = function(json,options){ 
		//各种属性、参数 
		var defaults = { 
			hideNegatives:false,//是否不显示无结果的区域
			caseSensitive:false//是否区分大小写
			
		} 
		var jss = $.extend(defaults, options); 
		//处理字符串
		//去掉空格
		var str=trim($(this).val());
		//转大写
		str=isCaseSensitive(str,jss.caseSensitive);
		//alert(str);
		//空格处切分
		var strArray = str.split(" ");
		//创建一个新的json返回，
		//alert("strArray = "+strArray);
		var newJson=[];
		//遍历json
		if(jss.hideNegatives){
			$(json).each(function(index){
				var obj=json[index];
				if(strArray.length==0){
					return newJson;
					//alert("空 = "+str);
				}else if(strArray.length==1){
					//alert("str = "+str);
					var newTitle=isCaseSensitive(obj.title,jss.caseSensitive);
					if((newTitle).indexOf(str)!=-1){//根据情况修改  obj.title
						obj["sortMark"]=1;
						newJson.push(obj);
					}
					
				}else{
					var markCount =0;
					for(var i=0;i<strArray.length;i++){
						//alert(strArray[i]);
						var newTitle=isCaseSensitive(obj.title,jss.caseSensitive);
						if((newTitle).indexOf(trim(strArray[i]))!=-1){//根据情况修改  obj.title
							markCount=markCount+1;
						}
					}
					if(markCount>0){
						//alert("markCount = "+markCount);
						obj["sortMark"]=markCount;
						newJson.push(obj);
					}
					
				}
			
			});	
		}else{
			$(json).each(function(index){
				var obj=json[index];
				if(strArray.length==0){
					return json;
					//alert("空 = "+str);
				}else if(strArray.length==1){
					//alert("str = "+str);
					var newTitle=isCaseSensitive(obj.title,jss.caseSensitive);
					if((newTitle).indexOf(str)!=-1){//根据情况修改  obj.title
						obj["sortMark"]=1;
					}else{
						obj["sortMark"]=0;
					}
					newJson.push(obj);
				}else{
					var markCount =0;
					for(var i=0;i<strArray.length;i++){
						//alert(strArray[i]);
						var newTitle=isCaseSensitive(obj.title,jss.caseSensitive);
						if((newTitle).indexOf(trim(strArray[i]))!=-1){//根据情况修改  obj.title
							markCount=markCount+1;
						}
					}
					obj["sortMark"]=markCount;
					newJson.push(obj);
					
				}
			
			});
			
		}
		//alert("newJson1 = "+newJson.length);
		newJson.sort(down);
		//alert("newJson2 = "+newJson.length);
		return newJson;
	} 

})(jQuery); 
//排序--降序
function down(x, y) {
    return (x.sortMark < y.sortMark) ? 1 : -1;
 
}
//删除左右两端的空格
function trim(str){ 
	return str.replace(/(^\s*)|(\s*$)/g, "");
}
//是否区分大小写  true：区分，false：不区分
function isCaseSensitive(str,isFalse){
	if(!isFalse){
		return str.toUpperCase();
	}
	return str;
}
